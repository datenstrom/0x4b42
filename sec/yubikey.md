# Yubikey

Date created: 2020-10-14 09:27:58

## Management Software

```bash
sudo apt-get install yubikey-manager
```

## OpenSSH

- Supports both RSA and ECC
- The private key can be generated on-chip

### Generate Key-Pair

**Note:** YubiKeys support curve 25519.

Generate a 384-bit ECC key pair on the YubiKey.

```bash
ykman piv generate-key -a ECCP384 9a pubkey.pem
```

Use the YubiKey to generate a self-signed certificate for the public key that expires in 5 years.

```bash
ykman piv generate-certificate -d 1826 -s "Key Name" 9a pubkey.pem
```

### Public Key Conversion

The `pubkey.pem` file contains the public key in PEM (Privacy Enhanced Mail) format. OpenSSH uses a different format defined in RFC 4253, section 6.6, so the PEM formatted key should be converted to the format OpenSSH understands. This can be done using `ssh-keygen`:

```bash
ssh-keygen -i -m PKC8 -f pubkey.pem > pubkey.pub
```

### Client Config

Install `opensc`:

```bash
sudo apt-get install opensc
```

In `~/.ssh/config`, note that arch may change:

```
PKCS11Provider /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so
```

### Server Config

```bash
ssh-copy-id -f -i ~/.ssh/my-key.pub my-server.starshell.ai
```