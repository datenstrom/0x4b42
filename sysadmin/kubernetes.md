# Kubernetes

Date created: 2020-09-20 12:51:04

## Terminology

**Pod**
- Run containers
- A space for containers to run with shared resources
    - Storage
    - Network
    - Compute Resources (cpu, memory, disk)
    - Environment Variables
    - Readiness and Health Checking
    - Network (IP address shared by containers in the Pod)
    - Mounting Shared Configuration and Secrets
    - Mounting Storage Volumes
    - Initialization
- Each receives own IP address
- Can communicate only within the pod using `localhost`
- Basically a virtualized physical machine

Once a pod is assigned to a node it stays there until it stops or is terminated.

**Node**
- An actual physical or virtual machine
- Runs pods

A node contains:
- The kubelet
- A container runtime
- The kube-proxy

**StatefulSets**

StatefulSets are valuable for applications that require one or more of the following.

- Stable, unique network identifiers.
- Stable, persistent storage.
- Ordered, graceful deployment and scaling.
- Ordered, automated rolling updates.


## Workloads

Higher level abstractions that run pods. Handle concerns such as replication, identity, persistent storage, custom scheduling, rolling updates, etc.

- Deployments (Stateless Applications)
  - replication + rollouts
- StatefulSets (Stateful Applications)
  - replication + rollouts + persistent storage + identity
- Jobs (Batch Work)
  - run to completion
- CronJobs (Scheduled Batch Work)
  - scheduled run to completion
- DaemonSets (Per-Machine)
  - per-Node scheduling

## Setup Docker Registry

Setup docker registry:

```bash
microk8s.enable ingress registry

sudo apt-get install apache2-utils
htpasswd auth datenstrom

microk8s.kubectl create secret generic basic-auth --namespace=container-registry --from-file=auth 
```

Verify registry is exposed on port `32000`:

```bash
=== ~ » microk8s.kubectl get all --namespace container-registry
NAME                            READY   STATUS    RESTARTS   AGE
pod/registry-7cf58dcdcc-92phm   1/1     Running   0          39m

NAME               TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service/registry   NodePort   10.152.183.156   <none>        5000:32000/TCP   39m

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/registry   1/1     1            1           39m

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/registry-7cf58dcdcc   1         1         1       39m
```

Set up TLS:

```bash
docker run -v ${pwd}/letsencrypt:/etc/letsencrypt -it certbot/certbot certonly --manual --preferred-challenges dns

kubectl create secret docker-registry docker-registry.b2-4ac.com --docker-username=<user> --docker-password=<password>

docker login anton.starshell.ai
```

Configure Ingress by creating `docker_registry_ingress.yaml`:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: registry
  namespace: container-registry
  annotations:
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Registry Authentication Required anton.starshell.ai'
    nginx.ingress.kubernetes.io/proxy-body-size: "0"
spec:
  rules:
  - host: anton.starshell.ai
    http:
      paths:
      - backend:
          serviceName: registry
          servicePort: 32000
  tls:
  - hosts:
    - anton.starshell.ai
    secretName: tlsecret
```

Apply the configuration:

```bash
microk8s.kubectl apply -f docker_registry_ingress.yaml
```