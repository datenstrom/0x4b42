# Kubeflow

Date Created: 2020-09-26 23:14:50

## MicroK8s

### Dashboard Auth Issue

Unable to access kubeflow dashboard from outside the cluster on 1.18 stable.

- [unable to access kubeflow-dashboard from outside the cluster #974](https://github.com/kubeflow/manifests/issues/974)

Found working solution in [issue #1140](https://github.com/ubuntu/microk8s/issues/1140). Specifically the [solution proposed by bipinm](https://github.com/ubuntu/microk8s/issues/1140#issuecomment-660044973) worked. This is still not a good solution as it requires everyone who needs access to edit their `/etc/hosts` file. It doesn't work with local DNS either so a static IP must be set which should probably be done anyway.

### User Management

See [microk8s issue #1116](https://github.com/ubuntu/microk8s/issues/1116).

### Fairing

