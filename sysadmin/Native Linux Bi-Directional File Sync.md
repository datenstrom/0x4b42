 Native Linux Bi-Directional File Sync

date created: 2020-07-03 17:19:34

## Goals

- Sync files between multiple computers
- Delete files when they are deleted from sync or any computer
- Handle conflicts

## `rsync` and `lsyncd`

File sync on Linux with the following properties can easily be achieved using `rsync` and `lsyncd`.

Pros:
- Easy to install (in package manager)
- Well documented

Cons:
- Sync is one way
- Does not handle conflicts
- Does not handle multiple platforms

## Unison

Another file-sync tool found [here](https://www.cis.upenn.edu/~bcpierce/unison/).

Pros:
- Easy to install (in package manager)
- Handles conflicts
- Will work for multiple platforms

Cons:
- Less popular/documented
- Not an online solution, must be manually ran

## RClone

[RClone](https://rclone.org/) definitely looks like a good solution from the [local filesystem docs](https://rclone.org/local/)… except sync is only one way. Looks like a possible encrypted backup solution though.

## NextCloud it is then