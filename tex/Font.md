# Tex Fonts

Date created: 2020-07-05 08:56:07

Upright serif `\mathrm`: $\mathrm{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Upright serif with spaces `\textrm`: $\textrm{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Bold upright serif `\mathbf`: $\mathbf{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Upright sans-serif `\mathsf`: $\mathsf{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Teletype `\mathtt`: $\mathtt{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Math blackboard bold `\mathbb`: $\mathbb{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Math Caligraphic `\mathcal`: $\mathcal{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$
Math Fraktur `\mathfrak`: $\mathfrak{The quick brown fox jumped over the log. \alpha\beta\gamma\phi\Phi}$