# Autonomy OS

An operating system, or userspace OS extension layer similar to ROS.

## Name

- Arcos: Autonomous Robotics (crossplatform) Operating System

## Technologies

1. Replace C with Rust
2. Erlang? It is very popular in communications.
    - [The Zen of Erlang (2016)](https://news.ycombinator.com/item?id=23888497) HN Discussion