# A collection of HN discussions on AI

Date created: 2020-07-04 10:05:05
Last modified: 2020-10-11 13:53:49

## [Object Detection from 9 FPS to 650 FPS](https://paulbridger.com/posts/video_analytics_pipeline_tuning/)

A great writeup about going from model development to production.

See [GPU Inference Speed for]([[2020-09-26 16:00:05]]) for implications.

## [The Overfitted Brain: Dreams evolved to assist generalization ](https://news.ycombinator.com/item?id=23956715)

This links to "Why we sleep?" by Matthew Walker and my own thoughts about what dreams may be.

## [Neural Supersampling for Real-Time Rendering](https://news.ycombinator.com/item?id=23714977)

This discussion looks like it contains methods that could be applied to data augmentation during training or even inference. We know that our own minds will fill in the gaps and possibly upsample our own vision.

## [The cost to train AI systems is improving at 50x the pace of Moore's Law](https://news.ycombinator.com/item?id=23738156)

Some useful methods and tricks in the discussion.

## [Powerful AI Can Now Be Trained on a Single Computer](https://news.ycombinator.com/item?id=23875367)

- High throughput asynchronous reinforcement learning.
- Single-machine policy gradient implementation.
- Multi-agent and population-based training experiments.
- VizDoom and DMLab wrappers.
- Compute specs:
    - First PC: 36-core CPU, Single RTX 2080 Ti GPU
    - Second PC: 10-core CPU, Single GTX 1080 Ti GPU

Paper: [Sample Factory: Egocentric 3D Control from Pixels at 100000 FPS with Asynchronous Reinforcement Learning](https://arxiv.org/abs/2006.11751)
Code: [alex-petrenko/sample-factory](https://github.com/alex-petrenko/sample-factory)

Contradicts the current trend of compute for SOTA methods [doubling every few months](https://openai.com/blog/ai-and-compute/). Article also has some nice charts on increase in compute for various methods from the 60's to 2018.

Another interesting find here was a reference to [Facebooks Habitat](https://aihabitat.org/) a simulation platform with photorealistic environments supporting multiple indoor tasks.