# Reinforcement Learning

Date created: 2020-07-18 09:47:34

## Definitions

- TD: Temporal Difference
- DQN: Deep Q-Network

## Notation

- $\textbf{w}$: Function parameters (weights)
- $\textbf{w}^-$: A copy of the function parameters (weights) fixed for a learning step

## Equations

## Readings

- [Paper describing Deep Q-Networks](https://www.cs.swarthmore.edu/~meeden/cs63/s15/nature15a.pdf)
- [Research paper that first introduced the Deep Q-Learning algorithm](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)