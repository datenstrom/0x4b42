# Deep Q-Networks

Date created: 2020-07-18 14:12:47

Deep Q-Learning represents the optimal action-value function $q^*$ as a neural network instead of a table. RL is [notoriously unstable](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.73.3097&rep=rep1&type=pdf) when ANNs are used to represent action values ut Deep Q-Learning addresses this using:

- Experience Replay
- Fixed Q-Targets

- DQN algorithm uses two separate networks with identical architectures
- The target Q-Network weights are updated less often (or more slowly) than the primary Q-Network
- Without fixed Q-targets, we encounter a harmful form of correlation, whereby we shift the parameters of the network based on a constantly moving target


![dqn.png](/home/datenstrom/Downloads/dqn.png)

Seminal DQN paper: [Human-level control through deep reinforcementlearning](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)

## Experience Replay

Not a new concept. Based on the idea that we can learn better, if we do multiple passes over the same experience. Builds a database of samples and then learns from them, in a way framing part of RL as supervised learning. Prioritized experience replay can also prioritize tuples that are rare or more important. Experience replay is used to generate uncorrelated experience data for online training of deep RL agents.

Addresses correlation between consecutive experience tuples by sampling them randomly and out of order.

## Fixed Q-Targets

Q learning is a form of Temporal Difference learning.

$$
\Delta\mathbf{w} =
\alpha\left(
	R +
    \gamma\max\limits_a \hat{q}(S^\prime,a,\mathbf{w}) -
    \hat{q}(S, A, \mathbf{w})
\right)
\nabla_\mathbf{w}\hat{q}(S,A,\mathbf{w})
$$

Our goal is to minimize the difference between between the target and the currently predicted Q value or the TD error. The TD target is supposed to be a replacement for the true value function $q_\pi(S,A)$ which is unknown.

$$
\Delta\mathbf{w} =
\alpha\bigl(
	\underbrace{
        \underbrace{
            R +
            \gamma\max\limits_a \hat{q}(S^\prime,a,\mathbf{w})
        }_\text{TD target} -
        \underbrace{
            \hat{q}(S, A, \mathbf{w})
        }_\text{current value}
    }_\text{TD error}
\bigl)
\nabla_\mathbf{w}\hat{q}(S,A,\mathbf{w})
$$

Addresses correlation between the target and the parameters we are changing. Here we update the weights, moving the target, which can cause thrashing. Each step will affect the target moving it in a complex and unpredictable way making convergence hard or impossible. To solve the problem fix the function parameters used to generate the target during the learning step.

## Deep Q-Learning Algorithm

1. Sample environment, store examples in replay memory
2. Obtain random mini-batches of tuples and learn from them using a GD update step

These two steps are not dependent on eachother and can be combined in many different ways.

### Algorithm

Initialize:
- Space for replay memory $D$ with capacity $N$
- Action-value function $\hat{q}$   with random weights $\mathbf{w}$
- Target action-value weights $\mathbf{w}^-\leftarrow\mathbf{w}$

For the episode $e\leftarrow 1$ to $M$:
- Initial input frame $x_1$
- Prepare initial state: $S\leftarrow\phi(\langle x_1\rangle)$

For time step $t\leftarrow 1$ to $T$:
- Sample:
    - Choose action $A$ from state $S$ using policy $\pi\leftarrow e\text{-Greedy}(\hat{q}(S,A,\mathbf{w}))$
    - Take action $A$, observe reward $R$, and next input frame $x_{t+1}$
    - Prepare next state: $S^\prime\leftarrow\phi(\langle x_{t-2}, x_{t-1}, x_t, x_{t+1}\rangle)$
    - Store experience tuple $(S,A,R,S^\prime)$ in replay memory $D$
    - $S\leftarrow S^\prime$
- Learn
    - Obtain random mini-batch of tuples $(s_j,a_j,r_j,s_{j+1})$ from $D$
    - Set target: $y_j=r_j+\gamma\max\limits_a\hat{q}(s_{j+1},a,\mathbf{w}^-)$
    - Update: $\Delta\mathbf{w}=\alpha\bigl(y_j-\hat{q}(s_f,a_f,\mathbf{w})\bigr)\nabla_\mathbf{w}\hat{q}(s_j,a_j,\mathbf{w})$
    - Every $C$ steps, reset $\mathbf{w}^-\leftarrow\mathbf{w}$