# RL in Continuous Spaces

Date created: 2020-07-18 13:02:20

## Tile Coding

This is simply creating a grid world from a continuous space.

## Coarse Coding

Take the world and draw many circles where a state will fall into many spheres. When the radius of a sphere is increased generalization is increased. The spheres, or generic container, shape can also be changed. Each circle can then be viewed as active or not based on if it contains the current state, and we can even use the distance from the center as a measure of how active the feature is. The measure can be smoothed using a Gaussian centered on the circle (Radial Basis Function). While this returns the state vector to a continuous space it can also **drastically** reduce the number of features.

> Note: This seems similar to embeddings and I wonder if this approach has been used to reduce the number of features for them, and if so how successful it was.

## Function Approximation

A disadvantage to discreetizing  continuous spaces is that the number of discrete states required to solve complex problems can become very large.

What we want:

$$
\text{True state value function: }\quad
\hat{v}(s) \approx v_\pi(s)
$$

$$
\text{True action value function: }\quad
\hat{q}(s, a) \approx q_\pi(s, a)
$$

These are usually smooth and continuous over the entire space and it is not tractable to find them. One approach to approximating the function is to add a parameter $\mathbf{w}$ that shapes the function.

$$
\text{Approximate state value function: }\quad
\hat{v}(s, \mathbf{w}) \approx v_\pi(s)
$$

$$
\text{Approximate action value function: }\quad
\hat{q}(s, a, \mathbf{w}) \approx q_\pi(s, a)
$$

Here the goal is finding a set of parameters that yield an optimal value function using Monte-Carlo, Temporal-Difference, etc.

### Linear Function Approximation

A linear function is a sum over all of the features multiplied by their weights (dot product). We can use gradient descent to optimize.

$$
\text{Value function: }\quad
\hat{v}(s, \mathbf{w}) =
\mathbf{x}(s)^\top \cdot \mathbf{w}
$$

$$
\text{Minimize error: }\quad
J(\mathbf{w}) =
\mathbb{E}_\pi
\left[
	\left(
    	v_\pi(s) -
        \mathbf{x}(s)^\top\mathbf{w}
    \right)^2
\right]
$$

$$
\text{Error gradient: }\quad
\nabla_\mathbf{w} J(\mathbf{w}) =
-2 \left(
	v_\pi(s) -
    \mathbf{x}(s)^\top \mathbf{w}
\right)
\mathbf{x}(s)
$$

$$
\text{Update rule: }\quad
\Delta\mathbf{w} =
-\alpha\frac{1}{2} \nabla_\mathbf{w} J(\mathbf{w}) =
\alpha \left(
	v_\pi(s) -
    \mathbf{x}(s)^\top \mathbf{w}
\right)\mathbf{x}(s)
$$

Limitations:
- Can only represent linear relationships between inputs and outputs
    - 1D: a line
    - 2D: a plane

### Kernel Function

A modification to linear function approximation that can be used to capture non-linear relationships. Kernel functions (or basis functions) transform the input sate into a different space. Radial Basis Functions are commonly used for this.

### Non-Linear Function Approximation

In linear function approximation with kernels our output value is still linear with respect to the features. What if the underlying value function was non-linear with respect to a combination of the feature values? We can pass the result of the dot-product through some non-linear function $f$ to make it non-linear.

$$
\text{Value function: }\quad
\hat{v}(s, \mathbf{w}) =
f\left(
	\mathbf{x}(s)^\top \cdot \mathbf{w}
\right)
$$

This should look familiar as it is the basis of ANN structure and is usually called an activation function.