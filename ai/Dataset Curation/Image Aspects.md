# Image Aspects

Date created: 2020-10-04 12:28:18

## Prior Art

- [SUN Attribute Database](http://cs.brown.edu/~gmpatter/sunattributes.html)

## Data Sources

There are many open datasets with data or metadata that could be used to extract image aspect labels. It may even be possible to scrape data and use context to infer labels. This approach will be messy however and require either a robust learning approach to deal with missing/incorrect labels or a more sophisticated approach such as Bayesian Neural Networks that can recognize when they are uncertain about labels and ask for clarification.

## Aspect Dataset

Multi-label classification dataset for image attributes that are commonly needed for
dataset diversification to avoid overfitting.

### Possible issues with the COCO data format when considering conflation:

The image IDs (and filenames) are sequential. If a UUID is used then no renaming would be required when merging separate datasets together. This would allow datasets to be both standalone and combined with reduced effort.

- Would a UUID, image hash, or [ULID](https://github.com/ulid/spec) be best?
- Hash would be redundant in a HashMap
- Hash as filename would prevent exact duplicates by nature
- UUID is not sequential ULID is
- UUID is better supported

### Data Format

The data is formatted for multi-label classification and stored using
JSON. At the top level the format is as follows:

```json
    {
      "info": info,
      "images": [image],
      "origins": [origin],
      "categories": [category]
    }
```

For individual fields:

```json
    info {
      "year": int,
      "version": str,
      "description": str,
      "date_created": datetime,
    }

    image {
      "id": int,
      "width": int,
      "height": int,
      "file_name": str,
      "category_ids": [int],
      "origin_id": int,
      "origin_filename": str,
      "date_captured": datetime,
    }

    origin {
      "id": int,
      "name" str,
    }

    category {
      "id": int,
      "name": str,
      "supercategory": str
    }
```