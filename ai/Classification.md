# Classification

Date created: 2020-10-12 09:13:49

## Types of classification problems

| Type                        | Predicted Labels                                | Possible Labels                                                                                                                       | Activation Function | Loss Function        |
|:---------------------------:|-------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|---------------------|----------------------|
| Multi-Class Classification  | smiling                                         | [neutral, smiling, sad]                                                                                                               | Softmax             | Cross Entropy        |
| Multi-Output Classification | woman, smiling, brown hair                      | [man, woman, child] [neutral, smiling, sad] [brown hair, red hair, blond hair, black hair]                                            |                     |                      |
| Multi-Label Classification  | portrait, woman, smiling, brown hair, wavy hair | [portrait, nature, landscape, selfie, man, woman, child, neutral emotion, smiling, sad, brown hair, red hair, blond hair, black hair] | Sigmoid             | Binary Cross Entropy |