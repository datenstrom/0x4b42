# Jeff Clune (Uber AI Labs) Guest Lecture

Date created: 2020-07-10 11:29:16

PNAS: Automated animal identification
- Automated Ecological Understanding (Norouzzadeh et al. 2018)
- Motion sensor cameras + deep learning
- 17,000 human hours to label 3.2 million images
    - Automated 99.3% with human-level accuracy with DNNs
    - Stop poaching
    - Protect endangered species
    - Transform ecology
    
    Papers focused on how much DNNs understand about images.
    - How transferable are features in deep neural networks (NeurIPS 2014)
    - Deep neural networks are easily fooled. (CVPR 2015)
    - Deep visualization. (ICML Deep Learning workshop 2015)
    - Synthesizing the preferred inputs for neurons in neural networks via deep generator networks. (NeurIPS. 2016)
    - Plug & Play generative networks. (CVPR 2017)
    - Convergent Learning: Do differential neural networks learn the same representations? (ICLR 2016)

Go-Explore (Ecoffet et al. 2019)
- Solves Montezuma's Revenge
- Exploration in RL

## AGI

Right now it looks like we are trying to identify the building blocks.
- How many are there?
- Can we find them all?

If we find them all we still need to find out how to put them together.
- Complex, non-linear interactions
- Debugging, optimizing would be a nightmare
- Massive team required (e.g. CERN)

There is a current trend of hand-designed pipelines being outperformed by learned solutions.

- Features
    - HOG/SIFT -> Deep Learning
- Architectures
    - Hand designed -> Learned (NAS)
- Hyperparmeters & data augmentation
    - Manually tuned -> Learned
- RL Algorithms
    - Hand designed -> Meta-Learning

AI-Generating ALgorithms (Clune 2019)

- Learn as much as possible
- Bootstrap from simple to AGI
- Expensive Outer-loop 
    - Produces a sample-efficient intelligent agent
- Existence proof
    - Earth

Things needed:

1. Meta-learn architectures
2. Meta-learn learning algorithms
3. Generate effective learning environments

AI-GA still requires building blocks but not as many. They will require tremendous compute.

## Learning Architectures

(Generative Teaching Networks (in review, ICLR))

Architecture matters a lot. This paper looks at speeding up NAS.

Train faster by scheduling:
- Curriculum Learning (Graves et al. 2017)
- Learning to teach (Fan et al. 2018)

Humans do not always learn with real data:
- Watching
- Reading
- Drills

Teaching methods can improve over time.

Meta-learn to generate data that enables rapid learning:
- Hyper-gradients (Maclaurin et. al. 2015)
- Data-set distillation (Wang et al. 2018)
- Generative Teaching Networks (GTN)
    - Few inner loop steps
    - Learn inner loop hyperparams
    - Stabilizing Meta-Learning with Weight Normalization (NeurIPS 2016)
    - 97% on MNIST
    - Adding learned curriculum helps significantly
    - Applying this to NAS on CIFAR was 4x faster than real data
    - This method combines with other NAS methods because they tend to use real data
    - Could be applied to RL


## Meta-Learn Learning Algorithms

Two meta-learning algorithm camps:
1. Meta-learn good initial weights + SGD (MAML)
2. Meta-learn RNN, which creates its own learning algorithm. (learning to RL, LRL, Wang et al. 2016) (RL^2, Duan et al. 2016)

LRL learns to:
- Explore
- Exploit

These methods store data in the activations which is not a good place to store them. Work to store data in weights instead is Differentiable Hebbian Learning (Differentiable plasticity: training plastic neural networks with backpropigation, Miconi, Clune, Stanley. ICML 2018).

- Can store info in weights and activations
- Hebbian learning (trained via SGD)
- Like LRL/RL^2, no (external) learning algorithm at meta test time.

Let's SGD control $\alpha$ so that it can use some parts for memory or whatever is required. Sometimes outperforms LSTMs. Near SOTA in 2018.

Differentiable Nuromodulated Plasticity: (Backpropamine, Miconi, Rawal, Clune, Stanley, 2018).

**Online-aware Meta-Learning (OML)** Javed & white 2019.
- Performs very well
- After sequentially training on 150 classes of Omniglot
    - 97% on meta-test training set (near perfect memorization)
    - ~63% on meta-test test set (worse at generalizing, but impressive)
- Learns a sparse representation
- Does well but still suffers from SGD induced CF

- A Neuromodulated Meta-Learning (ANML) Algorithm
    - OML with neuromodulation
    - Directly modulate activations: selective activation
    - Indirectly modulate learning: selective plasticity

Normal DL:
- IID sampling (no catastrophic forgetting)
- Multiple passes through data

Sequential Learning:
- Catastrophic Forgetting
- One pass through data

ANML outperformed on meta-test.

Future work:
- Improve performance
- Try replacing SGD entirely
    - e.g. with differential Hebbian plasticity or backpropamine

## Generate Effective Learning Environments

- Where will we get them?
- Will they be the right ones to catalyze learning?
- Can algorithms generate their own problems while learning to solve them?

Open-Ended Algorithms
- Endlessly innovate
- Examples
    - Natural evolution
    - Human culture
- Can we make algorithms that do this?

Paired Open-Ended Trailblazer (POET) (GECCO 2019) Endlessly generating increasingly complex and diverse learning environments and their solutions.
- Periodically Generate new learning environments
    - Add to population IF
    - Novel
    - Not too easy, not too hard
- Optimize agents to better solve each one
    - Allow goal-switching

Paradox: Novelty search (Lehman & Stanley)
- Maximize reward: Fails
- Maximize novelty: Succeeds

Goal Switching (Nguyen, Yosinski & Clune 2016)
- Optimize related tasks also
- Gets agents out of local optima
- Periodically transfer an agent from one domain to another with transfer learning
- Environments are gradually harder
- Evolutionary evolving environments
- Invented curriculum
- Agents put directly into hard environments never learn them
- Manually designed curriculum did not succeed

Learning Curricula:
- Open-endedness
- Procedural content generation
- Quality diversity
- Developmental robotics
- Co-evolution/self-play
- GANs
- Minimal criterion co-evolution
- Direct curriculum learning methods

More complex environments:
- Heess et al. 2017
- Bansal et al. 2017
- POET in a game !!!!!!!

POET-generated tasks for meta-learning
- Single distribution (each task sampled)
- Many distributions (each task a distribution over env type)

POET is similar to Automatic Domain Randomization (ADR) like used in Learning Dexterity.

Isn't the real world like this though? Or the internet?

## Lifelong Learning

Catastrophic Forgetting
- Achilles heel of machine learning
- New tasks overwrite old ones

Literature (all manually designed):
- Catastrophic inference in connectionist networks: the sequential learning problem (1989)
- Connectionist models of recognition memory: Constraints imposed by learning and forgetting functions (1990)
- Catastrophic forgetting in connectionist networks (1999)
- Progressive Networks (2016)
- Elastic Weight Consolidation (2016)
- Replay
    - Experience replay
    - Generative replay (Kamra et al. 2017)
- Knowledge Distillation
    - Progress & Compress (Schwarz et al. 2018)
    - Learning without Forgetting (Li & Hoiem 2017)
- Gated Mixture-of-Experts
    - PathNet (Fernando et al. 2017)
    - Outrageously Large Neural Networks (Shazeer et al. 2018)

> Hypothesis: there's a good chance humans are not smart enough to manually build systems that continually learn well.

I doubt this is true, with enough time and experimentation it will be found eventually.

But a better path is probably to use Meta-Learning to optimize for continual learning.

