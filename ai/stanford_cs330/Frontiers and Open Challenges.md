# Frontiers and Open Challenges

Date created: 2020-07-11 13:48:07

What doesn't work very well?
- How do we construct tasks for meta-learning?
    - Memorization problems
    - Can we use data without task boundaries?
    - Can the algorithm come up with tasks themselves?
- What does it take to run multi-task & meta-RL across distinct tasks?
    - How do we specify the task?
    - What set of distinct tasks do we train on?
    - What challenges arise?

## Constructing Tasks

For image classification we divide the dataset up into a meta-dataset and train it randomly sampling the categories.

What would happen if we didn't shuffle the categories? For example the semantic category "cat, dog, bird" always corresponds to the same task ID "1, 2, 3, 4."
- The meta learning algorithm would use the task ID as information
- The meta learning algorithm would not generalize well

How do we construct distributions of tasks for meta-learning?
(Yin, Tucker, Yuan, Levine, Finn. Meta-Learning without memorization.'19)
- Shows that the learner can ignore the task data $\mathcal{D}^\text{tr}$
- This works for the data in the training data
- It will completely fail at meta-test time

The **memorization problem**: can memorize an aspect of the training dataset that does not allow it to generalize to the test dataset.

If tasks are mutually exclusive: single function can not solve all tasks
- i.e. due to task shuffling, hiding information

If tasks are non-mutually exclusive: single function can solve all tasks
- Multiple solutions to the meta-learning problem

1. Memorize all cononical pose info in $\theta$ and ignore $\mathcal{D}^\text{tr}_i$
2. Carry **no** info about canonical pose info in $\theta$ and acquire from $\mathcal{D}^\text{tr}_i$
3. An entire spectrum of solutions based on how information flows

This situation arises when the mutual information is a function between the prediction and training data given the input and theta are equal to zero. The prediction is only dependent between theta and x.

$$
\mathcal{I}
(\hat{y}; \mathcal{D}^\text{tr} | x, \theta) = 0
$$

A potential solution is to control the information flow.

**Meta-Regularization** MR: minimize meta-training loss + information $\theta$
Place precedence on using information $\mathcal{D}^\text{tr}$ over $\theta$.

(TAML: Jamal & Qi. Task-Agnostic Meta-Learning for Few-Shot Learning. CVPR'19)

## Data without task boundaries

We just have a time-series of data and we want to learn form it.
- Energy demand
- Dynamics of robot/car
- Predict transportation usage
- Stock market
- Video analytics
- RL agent

These are all unsegmented but have significant temporal structure. So can we segment the time series into tasks & meta-learn across tasks? How do we segment?
- Bayesian online change point detection (BOCPD) Adams & Mackay'17
- BOCPD is differentiable, we can backprop through update belief update to meta-train model
    - Meta-Learning with Online Changepoint Analysis (MOCA)
        - Meta-training phase: given unsegmented time-series of offline data
        - Meta-test phase: Streaming online learning & prediction
        - Created a streaming variant of Mini-ImageNet

## Meta-Learn with only unlabeled image?

Construct tasks with unlabeled data.
- We need to group the images
- Label the groups

(Hsu, Levine, Finn. Unsupervised Learning via Meta-Learning. ICLR'18)

We cannot do anything like group the data in pixel-space or run k-means in pixel-space, but we can use unsupervised learning to get an embedding space. Then we can propose tasks using clustering to sample in the embedding space, creating different selections in the embedding space and generate tags for them for both train and test sets. Then we can run our favorite meta-learning algorithm.

Result: representation is very suitable for learning downstream tasks.

The number of tasks corresponds to the number of clusters chosen the number of ways. So 5-way classification with 1000 clusters is 1000 chose 5 tasks.

The clusters will not correspond to actual semantic classes (they found ones that correspond to pairs of object or round objects for example) but they dont need to, what is important is that they correspond to structured aspects s.t about the images such that the learner learns to quickly adapt at test time to new structured aspects (that do correspond to semantic labels).

Embedding space:
- BiGan – Donahue et al.'17
- DeepCluster – Caron et al.'18

Propose tasks:
- Clustering to automatically construct tasks for unsupervised meta-learning (CACTUs)

Run meta-learning:
- MAML – Finn et al.'17
- ProtoNets – Snell et al.'17

Mini-ImageNet 5-way 5-shot:
- MAML with labels: 62.13%
- BiGAN kNN: 31.10%
- BiGAN logistic 33.91%
- BiGAN MLP + dropout: 29.06%
- BiGAN cluster matching: 29.49%
- BiGAN CACTUs MAML: 51.28%

Same story for:
- 4 different embedding methods
- 4 datasets (Omniglot, CelebA, Mini-ImageNet, MNIST)
- 2 meta-learning methods
- Test tasks with larger datasets (50-shot range)

Takeaways:

Can learn priors for few-shot adaptation using:
- Non-mutually exclusive tasks: through meta-regularization
- From unsegmented time series: via end-to-end changepoint detection
- From unlabeled data and experience: using clustering

This should make it _significantly_ easier to deploy meta-learning algorithms! It is also not the final solution to the problems, this is all very recent work and there are many more solutions to explore.

## What does it take to run multi-task & meta-RL across distinct skills

Have MAML, PEARL accomplished the goal of making policy adaptation fast?
- Sort of…
- Can we adapt to entirely new tasks?
    - We need meta-train to match meta-test
    - We need a broad distribution of tasks for meta-training

Can we meta-train across task families? Consider a space of manipulation tasks:
- Zhao, Jang, Kappler, Herzog, Khansari, Bai, Kalakrishnan, Levine, Finn. Watch-Try-Learn.'19
- Learns from one demonstration and a few trials
- Compared to meta-reinforcement learning (only using trials)
- Compared to meta imitation learning (one demonstration)
- Compared to behavior cloning across all tasks (no meta-learning)
- WTL learns across 4 distinct task families
- Significantly outperforms using only trials or only demos

WTL set-up: Demonstration only partially specifies the task, hence requires trials to identify the task. If the demo fully specifies the task it resolves to one-shot imitation learning and will ignore the trials during meta-training. A variant of the memorization problem.

Options for finding the needed broad (and not sparse)distribution of tasks:
- Brockman et al. OpenAI Gym. 2016
- Bellemare et al. Atari Learning Environment. 2016
- Fan et al. SURREAL: open-source reinforcement learning framework and robot manipulation benchmark. CoRL 2018

These are not great options because there is not exactly a clear underlying shared structure that can be used to learn tasks from the distribution. SURREAL has more structure but far fewer tasks.

We want: Meta-World CoRL'19
- 50+ qualitatively distinct tasks
- Shaped reward function & success metrics
- All tasks individually solvable (to allow us to focus on multi-task / meta-RL component)
- Unified state & action space, environment (to facilitate transfer)

Meta-World performance on meta-test:
- MAML: 23.93%
- RL^2: 20%
- PEARL: 30%

Performance is poor even on the 45 meta-training tasks. Multi-task RL algorithms also struggle. Why?

- Exploration challenge? No, all tasks are individually solvable.
- Data scarcity? No, all methods given budget with plenty of samples.
- Limited model capacity? No, all methods were given plenty of capacity.
- Training models _independently_ performs the best.
- Conclusion: must be an optimization challenge.

Ideas: (Gradient Surgery for Multi-Task Learning.'19)
- Gradients from different tasks are often in conflict
    - This can be diagnosed by looking for **negative inner product** of gradients (gradients pointing in different directions)
- When they do conflict, they cause more damage than expected
    - i.e. due to high curvature
    
Approach: when making a gradient step try to avoid making other tasks worse.
- If two gradients conflict (negative inner product): project each onto the normal plane of the other.
- Can still get worse under some conditions but even so should get "less worse"
- PCGrad: Project Conflicting Gradients

Results:
- Learns significantly faster and better than independent networks or weight sharing without PCGrad

Does this help for multi-task supervised learning?
- It looks like it does
- It combines well with other solutions
- Tested on Multi-Task CIFAR-100 and Multi-Task NYUv2

Why does it work well?
- It changes both the magnitude and direction of the gradient
- Tests showed that both mattered but direction mattered the most

Takeaways:
- Scaling to broad task distributions is hard, can't be taken for granted (from current algorithms)
    - Convey task information beyond reward (e.g. a demo)
    - Train on broad, dense task distributions like Meta-World
    - Avoid conflicting gradients
    
    
## Open Challenges (that haven't been covered)

Addressing fundamental problem assumptions:
- Generalization: meta-train and meta-test task distributions must be the same
    - Many out of distribution or long tail task distributions
        - Object encountered
        - Interactions with people
        - Words heard
        - Driving scenarios
    - We know how to do few-shot so we should be able to adapt to data in the tail
        - But the few-shot tasks are from a different distribution
    - Possible solutions
        - Domain adaptation literature
        - Robustness literature
    - Multimodality: Can you learn priors from multiple modalities of data?
- Algorithm, Model Selection: When will multi-task learning help you?
    - We need better benchmarks
        - Mini-ImageNet
        - Omniglot
        - Meta-Wrold (Yu et al.'19)
        - Meta-Dataset (Triantafillou et al.'19)
        - Visual Task Adaptation Benchmark (Zhai et al.'19)
        - Taskonomy Dataset (Zamir et al.'18)
        - Goal
            - Reflection of real world problems
            - Appropriate level of difficulty
            - Ease of use
        - Without benchmarks we can't make progress
    - Improving core algorithms
        - Computation and memory: Making large scale bi-level optimization practical
        - Theory: Develop a theoretical understanding of the performance of these algorithms
        - Multi-Step Problems: performing tasks in sequence presents challenges
        
        
## The big picture 
        
A lot of machine learning systems are very specialized and can only perform one narrow thing in one narrow environment.

Steps towards generalists:
- Learn multiple tasks (multi-task learning)
- Leverage prior experience when learning new things (meta-learning)
- Learn general-purpose models (model-based RL)
- Prepare for tasks before you know what they are (exploration, unsupervised learning)
- Perform tasks in sequence (hierarchical RL)
- Learn continuously (lifelong learning)

What is missing?