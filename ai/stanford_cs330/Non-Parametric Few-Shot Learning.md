# Non-Parametric Few-Shot Learning

Date created: 2020-07-04 09:44:01

Key idea: Can we use parametric meta-learners that produce effective
non-parametric learners?

- Expressive for most architectures
- Consistent under certain conditions
  - If the embedding is not missing important information
  - Works well with very large embeddings
- Entirely feedforward
- Computationally fast & easy to optimize
- Harder to generalize to varying K (but GPT)?
- Hard to scale to very large K (but GPT)?
- So far limited to classification

Simple and work well in low data regimes. During meta-test time FS learning
is in the low data regime, but during meta-training we still want to be
parametric and scale to large datasets.

Examples include:

- Siamese networks 
- Matching networks
- Prototypical networks

In what space do you compare? With what distance metric? Pixel space?
l2 distance? No we learn to compare with meta-training data.

Matching Networks (NeurIPS'16)
- Can we match meta-train & meta-test? Nearest neighbors in learned embedding space.
- Trained end-to-end
- If >1 shot performs many individual comparisons to find nearest

Prototypical Networks (NeurIPS'17)
- Aggregate class information to create a prototypical embedding

Idea: Embed, then nearest neighbors

- What if you need to reason about more complex relationships?
  - Learn non-linear relations module on embeddings (learns embedding and distance metric) (Relation Net)
  - Have an infinite mixture of prototypes per class (Allen et al. IMP, ICML'19)
  - Perform message passing on embeddings (Garcia & Bruna, GNN) [graph NN]