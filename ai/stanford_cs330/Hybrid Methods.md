# Hybrid

Date created: 2020-07-04 09:45:23

- Both condition on the data and run gradient descent (CAML'19)
- Gradient descent on relation net embedding (Rusu et al. LEO'19)
- MAML, but initialize last layer as ProtoNet during meta-training (ProtoMAML'19)