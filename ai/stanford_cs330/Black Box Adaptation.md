# Black box adaptation

Date created: 2020-07-04 09:43:18

Key idea: Find $\phi$ through inference.

- Complete expressive power
- Poor consistency
- Easy to combine with learning problems (SL, RL)
- Challenging optimization (no prior)
- Data-inefficient

Examples include:

- LSTMs & Neural turing machine (NTM)
- Conditional Neural Processes
- Meta Networks
- Convolutions and Attention
  - A Simple Neural Attentive Meta-Learner @DBLP:conf/iclr/MishraR0A18
  - 97-99% accuracy on omniglot


- Expressive, can represent any function
- Easy to combine with a variety of learning problems (SL, RL)
- Complex model
- Complex task: challenging optimization problem
- Often data-inefficient (taskwise)