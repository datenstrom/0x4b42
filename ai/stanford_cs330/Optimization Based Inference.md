# Optimization based inference

Date created: 2020-07-04 09:43:34

Key idea: Find phi through optimization.

- Consistent, reduces to GD
- Expressive for very deep models (except for some RL settings)
- Positive inductive bias
- Handles varying & large K well
- Model agnostic
- Unfortunately second-order optimization
- Usually compute and memory intensive

Examples include:

- Transfer learning
  - Initialization for fine tuning can be viewed as a prior (meta-parameters).
  - Learn parameter-vector such that they transfer well to new classes
  - Competative with black box
  - Even works with one-shot
- Model-Agnostic Meta-Learning: MAML @DBLP:conf/icml/FinnAL17
  - How to compute MAP estimate?
    - Gradient decent with early stopping = map inference under Gaussian prior (Santos'96)
    - Kind of: approximates hierarchical Bayesian inference
  - Other priors?
    - Gradient decent with explicit Gaussian prior: Rajeswaran et al. implicit MAML'19
    - Bayesian linear regression: Harrison et al. ALPaCA'18
    - Closed-form learned features: Bertinetto et al. R2-D2'19
    - SVM: Lee et al. MetaOpNet'19 (Was SOTA on few-shot at publication)

Optimization has shown to better adapt to tasks outside of the training
distribution (Finn & Levine ICLR'18).

Optimization based methods can represent any black-box adaptation given:

- sufficiently deep f
  - Possibly relaxed deepness with good priors
- Nonzero alpha
- Loss function gradient does not lose information about the label (MSE, Crossentropy loss)
- Data-points in training dataset are unique

Challenges:

- How do we choose architecture that is effective for inner gradient-step?
  - Progressive neural architectures search + MAML (Kim et al. Auto-Meta)
    - Finds highly non-standard architectures (deep & narrow)
    - Different from architectures that work well for standard supervised learning
    - Mini-ImageNet, 5-way 5-shot MAML: 63.11% -- MAML + AutoMeta: 74.65%
- Bi-level optimization can exhibit instabilities
  - Auto-learn inner vector learning rate, tune outer (Li et al. Meta-SGD, behl et al. AlphaMAML)
  - Optimize only a subset of the params in inner loop (DEML, CAVIA)
  - Decouple inner learning rate, BN stats per-step (MAML++)
  - Introduce context variables for increased expressive power (bias transformation, CAVIA)
- Backprop through many inner gradient steps is compute & memory intensive
  - (Crudely) approximate the jacobian as identity (first-order MAML'17, Nichol et al. Reptile'18)
    - Works for simple FS problems not more complex ones
  - Derive meta-gradient using the implicit function theorem (implicit MAML [iMAML])

Takeaways:

- Construct a bi-level optimization problem
- Positive inductive bias at start of meta-learning
  - Can expect it to do something reasonable at initialization
- Consistent procedure, tends to extrapolate better
- Maximally expressive with sufficiently deep network
- Model-agnostic (easy to combine with any architecture)
- Unfortunately requires second-order optimization
- Usually compute and/or memory intensive