# Bayesian meta-learning

Date created: 2020-07-04 09:46:02

Active area of research.

Bayesian Deep Learning (Stanford CS 236)

Parametric approaches use deterministic point estimates, this is a problem
when the problems are ambiguous even with a prior. For example a small set
of only young people, smiling, wearing hats. In such a case the model can
not tell if it is classifying people, people wearing hats, or people smiling.
To solve this can we learn to generate hypotheses about the underlying function?

**Important for:**
- Safety-critical FS learning (e.g. medical imaging)
- Learning to **actively learn**
- Learning to **explore** in meta-RL
- See _Active learning w/ meta-learning_
  - Woodward & Finn '16
  - Konyushkova et al. '17
  - Bachman et al. '17

One approach is to let f output the parameters of a distribution over y-test.

- Simple
- Can combine easily
- Can't reason about uncertainty over the underlying function
- Limited to class of distributions over y-test can be expressed

Toolbox:

- Latent variable models + variational inference
- Bayesian ensembles
- Bayesian neural networks
- Normalizing flows
- Energy-based models & GANs

Blackbox Bayesian See:

- Amortized Bayesian Meta-Learning (Ravi & Beatson'19)
  - Running gradient decent at test time
  - Quite Simple
  - But... Need to model phi given theta as a gaussian (reparamaterize for backprop)
- Ensemble of MAMLs (EMAML) (Kim et al. Bayesian MAML'18)
  - Does not work if ensembles are too similar
  - Stein Variational Gradient (BMAML)
    - Uses stein variational gradient descent (SVGD) to push particles away from eachother
    - Push different ensemble models away from eachother
    - Optimized for distribution of M particles to produce high likelihood
  - Simple
  - Tends to work well
  - gives non-gaussian distributions
  - But... requires M sets of parameters
    - Can be mitigated by only doing gradient based inference on last or last few layers


Optimization-based Bayesian:

- Probabilistic MAML (Finn, Xu, Levine '18)
  - Sample parameter vectors with a procedure like Hamiltonian Monte Carlo
  - Learn a prior where a random kick can put us in different modes
    - Add noise and run GD which will find multiple optima allowing adaptation to what was unknown (smile, hat, person)

## How to evaluate a Bayesian meta-learner

Standard benchmarks like Mini-ImageNet accuracy:

- Standardized so comparable to other papers
- Uses real images
- Good check that the approach did not break anything
- Evaluates the accuracy we care about
- Metric does not evaluate uncertainty
- Algorithms may be overfitting to benchmarks
- May not be much ambiguity in the underlying task
- Uncertainty may not be useful on this dataset

Toy Problems:
- Qualitative Evaluation on Toy Problems with Ambiguity (Finn, Xu, Levine, NeurIPS'18)

GAN example:
- Evaluation on Ambiguous Generation Tasks (Gordon et al. ICLR'19)

Likelihood & accuracy with distribution coverage:
- Accuracy, Mode coverage, & Likelihood on Ambiguous Tasks (Finn, Xu, Levine, NeurIPS'18)

Compare correlation of confidence level to the accuracy:
- Reliability Diagrams & Accuracy (Ravi & Beatson ICLR'19)

Active learning evaluation:
- Sinusoid Regression (Finn, Xu, Levine, NeurIPS'18)
  - Toy regression problem
- Mini-ImageNet (Kim et al. NeurIPS'18)