# Reinforcement Learning

Date created: 2020-07-04 09:48:38

When you do not need sequential decision making:
- When you are making a single isolated decision
  - Classification
  - Regression
- When the decision does not affect future inputs or decisions

Places where it is not good to ignore relation to future inputs and decisions:
- robotics
- Language & dialog
- autonomous driving
- business operations
- finance
- most deployed ML systems that interact with humans

Imitation Learning:
- Works well if a lot of expert data is available
- Tend to fail when it is a very long horizon system
- The system does not reason about outcomes in any way, only imitates the data

Reinforcement Learning:
- Takes a reward function and tells us which states or actions are better
- The s, a, r(s, a) define the Markov Decision Process
- Goal is to learn a policy to make predictions about actions
- Works for infinite and finite horizon cases

Reinforcement Learning Task (One way):
- Defined as Markov Decision Process
- Has:
  - state space
  - action space
  - initial state distribution
  - dynamics function
  - reward function
- Comparing to the supervised learning methods
  - Initial state distribution and dynamics correspond to the data generating distributions
  - The reward function corresponds to the loss funciton
  - The state and action space tell you the set your drawing from

Examples:
- Personalized recommendations
- Character animations
  - Across maneuvers
  - Across garments & initial states
- Same task across multiple robotic platforms (multiple arms)

Alternate view of RL task:
  - A task identifier is part of the state space
  - s = (s-bar, z-i)
  - Task ID can be
    - One hot
    - Language description
    - desired goal state -- this is "goal-conditioned RL"
  - Reward is the same
  - Reward for goal-conditioned can be something like
    - Negative distance between current state and goal state
    - Distance in space
    - Distance in latent space
    - Sparse 0/1 (zero when not goal)

**Goal conditioned** for learning what an anomaly is?

## Model-free RL methods (policy gradient, Q-Learning)

Policy gradients:
  - REINFORCE algorithm
  - In comparison to maximum likelihood for imitation learning
    - Looks very similar
    - Primary difference is the reward term
  - Very easy to do multi-task learning
  - Easy to combine with meta-learning such as MAML and black-box
    - Model-Agnostic Meta-Learning for Fast Adaptation of Deep Networks ICML'17
    - A Simple Neural Attentive Meta-Learner ICLR'18
      - Black box
      - Train on 1000 small mazes
      - Test on held-out small mazes and large mazes
      - Most complicated task as of '18
      - As of '19
        - Task able to run on entirely different robot
        - Settings where the task themselves are only partially observable
        - Manipulation tasks generalizing to entirely new manipulation tasks

**Note:** In the supervised setting MAML is very expressive, in the RL setting
it is not because of the policy gradient. For example if the reward function
is zero for all states then the gradient will always be zero, so even with
lots of experience with the environment, with zero reward it cannot
use that experience to update the policy. There are also other cases where
the policy gradient isn't very informative.

Although applying meta-learning policy methods is easy in comparison to
Q-Learning and actor-critic methods.

Pros:
- Simple
- Easy to combine with existing multi-task & meta-learning algorithms

Cons:
- Produces a high-variance gradient
  - Can be mitigated with **baselines** (used by all algorithms in practice)
  - Can also be mitigated with **trust regions**
- Requires on-policy data
  - Cannot reuse existing experience to estimate the gradient
    - **Importance weights** can help, but also high variance


## Value-based RL methods

Summary: The goal of value-based RL is to learn the Q function and to use that function to perform the task.

Review:
- Value function: Total reward starting from $s$ and following $\pi$
  - How good is a state
- Q function: Total reward starting from **s**, taking **a** and then following **pi**
  - How good is a state-action pair
- Set pi(a|s) <- 1 for a = argmax Q^pi(s, a^bar)
  - New policy is at least as good as old policy

Full fitted Q-iteration algorithm:
- Collect a dataset using some policy
- Hyperparams are the dataset size N and collection policy
- Then set the reward + max Q as some target label
- Improve policy to try to match target values (obtain phi)
- Other hyperparams iterations K and gradient steps S
- Results in policy pi(a|s) form argmax Q\_phi(s,a)
- Notes:
  - Can reuse data from previous policies
  - An off-policy algorithm, using replay buffers
  - Not gradient descent, dynamic programming algorithm (makes tricky to combine)
  - Relatively easy to combine with multi-task and goal-conditioned RL
    - Condition on task identifier or goal

These are analogous to multi-task supervised learning:
- Stratified sampling
- Soft/hard weight sharing

What is different?
- The data distribution is controlled by the agent.
- Should we share data in addition to weights?
- May know what aspects of the MDP are changing across tasks, can we leverage this?

This could be useful for online improvement of a data collection meta-learner:
> **Hindsight relabeling**: Consider the situation where you attempt to do some
> task and the outcome is good but corresponds to another task. For example you
> are playing hockey and attempt to shoot a goal but instead accidentally perform
> a good pass. We can relabel that experience with the pass identifier and store
> it with the reward for that task.
>
> Sometimes also called Hindsight Experience Replay (HER).
>
> Learning to achieve goals (IJCAI'93)
> Hindsight Experience Replay (Andrychowicz et al. NeurIPS'17)

In the HER'17 paper performance was greatly improved on robotic manipulation tasks.

How does this look:
- Collect data using some policy
- Store data in replay buffer
- Perform hindsight relabeling:
  - Relabel experience in dataset using last state as goal
  - store relabeled data in replay buffer
- update policy using replay buffer

Hindsight relabeling generally helps with exploration.

When can relabeling be applied?
- When the reward function form is known and evaluatable
  - If you can't evaluate in all possible contexts it may be hard to do this
  - If you have to ask a human it may be hard to do this
- Dynamics must be consistent across goals or tasks that you relabel for
  - The tuples (s, a, s+1) will no longer be dynamically consistent
- Need to use an off-policy algorithm...
  - Some people have experimented with on policy methods

Pros:
- Tend to be lower variance (by introducing meta-variance)
- Can use off-policy data

What about for image observations?
**recall**: Needs a distance function between current and goal state

$$r_t^\prime=-d(s_t,s_T)$$

Use binary 0/1 reward? Sparse but accurate and can still be useful.
**Could this be especially useful for say subcategory identification in hierarchical classification?**

Note that **random, unlabeled interaction** is **optimal** under the 0/1 reward of reaching the last state. If all we care about is reaching the last state, the goal state is the last step and we don't care about any of the other time steps or how we got there.

This makes dealing with images much easier. So if the data is **optimal**, can we use **supervised imitation learning**?

Algorithm:

1. Collect data $\mathcal{D}_k=\{(s_{1:T},\,a_{1:T}\}$ using some policy
2. Perform hindsight relabeling
  - Relabel experience in $\mathcal{D}_k^\prime=\{(s_{1:T},\,a_{1:T},\,s_T,\,r_{1:T}^\prime)\}$ where $r_t^\prime=-d(s_t,\,s_T)$
  - Store relabeled data in replay buffer $\mathcal{D}\leftarrow\mathcal{D}\cup\mathcal{D}_k^\prime$
3. Update policy using **supervised imitation** on replay buffer $\mathcal{D}$

One paper performed this task by interacting with a human in not random but directed ways. Collected data from "human play", and performed goal-conditioned imitation. "Learning Latent Plans from Play" '19

Can we also use this to **learn a better goal representation**? Which representation, when used as a reward function, will cause a planner to choose the observed actions? (Universal Planning Networks ICML'18) (Unsupervised Visuomotor Control through Distributional Planning Networks RSS'19)

1. Collect random, unlabeled interaction data:
2. Train a latent state representation $s\rightarrow x$ and latent state model $f(x^\prime) |x,a)$ such that if we plan a sequence of actions with respect to a goal state $s_t$, we recover the observed action sequence.
3. Throw away latent space model, and return goal representation $x$

This is referred to as "distributional planning networks": Performs a planning procedure inside a neural network and outputs a probability distribution over action sequences.

The metric this gives you is much more shaped, because the planner must be able to use the shaped reward function and it cannot succeed with a sparse reward function. When this metric is compared to other metrics used for robotic manipulation tasks such as:

- Metric from DPN
- Pixel distance
- Distance in VAE latent space
- Distance in **inverse model** latent space

This learned goal representation outperforms the others. Successfully used to learn policies to reach a target image and pushing objects.

## Exploration in RL Meta-Learning

Motivation: Learning approaches have excelled at creating agents that are specialists. We want to create agents that are generalists so they are able to exploit the structure in the world to learn new tasks faster. For example surfing, scateboarding, and snowboarding all require balance.

The difference between supervised meta-learning the RL agent has to collect adaptation data. For supervised learning adaptation is done using data given to us $\mathcal{D}_i^{\text{tr}}$ and in RL the agent needs to collect adaptation data for each task to derive $\mathcal{M}_i$. We aren't given the transition function or reward function that define $\mathcal{M}_i$ and we need to collect the data that will best allow you to adapt.

One approach is to run the REINFORCE algorithm. 

### PG Meta-RL Algorithms: Recurrent

Duan et al. 2016, Wang et al. 2016, Heess et al. 2015

To turn policy gradient into a Meta-RL algorithm we need to collect and remember information (experience) as we go and use it to inform new actions as we go. This sounds a lot like a RNN. To implement the policy as a recurrent network and train across a set of tasks. We also want to persist the hidden state across episode boundaries (episodes) for continued adaptation.

$$
\theta^\star =
\underbrace{\arg\max\limits_\theta}_{\mathclap{\text{PG outer loop}}}
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{RNN inner loop}}}
(\mathcal{M}_i)
$$

- Train with PG as usual
- $\theta^\star$ (the RNN) is a learning algorithm for a new task
    - Learned by optimizing $\theta$
    - Learning happens through the recurrent update of the hidden state
  
  Pros:
  - General
  - Expressive
 
 Cons:
 - Not consistent
 
 ### PG Meta-RL Algorithms: Gradients
 
Finn et al. 2017
 
What if we use a policy gradient on the inner loop also (MAML)? This way we find some parameters $\theta$ such that when we take a few gradient steps on that $\theta$ we reach $\theta^\star$ for that MDP.
 
$$
\theta^\star =
\underbrace{\arg\max\limits_\theta}_{\mathclap{\text{PG outer loop}}}
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{PG inner loop}}}
(\mathcal{M}_i)
$$

Pro:
- Consistent
    - Adaptation step is gradient descent
    - We know GD converges in non-linear (non-convex) settings to local minima
    
Cons:
- Not expressive as recurrent model
  - If rewards are sparse information is hard to obtain
      - If reward is 0 there is no PG update
          - Should have learned something about the dynamics
          - Should have learned that it is not a good place to go

### Exploration

How do these algorithms learn to explore:
- Backprop through time
- Credit assignment
- Rothfuss et al. 2018

How well do they explore?
- Duan et al. 2016
- ProMP Rothfuss et al. 2017
- Gradient-based meta-RL fails to explore in a sparse reward navigation task
    - MAESN. Gupta et al. 2018

Note: combining these with off-policy RL does not work well or at all in nearly all research so far but is an active area of research.

Problem: exploration methods often add noise in places that are not meaningful. For example a robot randomly exploring joint space instead of exploring interacting with an object in meaningful ways. Exploration policies are required to be stochastic while optimal behavior is deterministic putting them at odds. It can be hard to represent both exploration and optimal behavior in the same policy especially when methods of adding noise are time-invariant.

Solution: Temporally extended exploration

$$
\theta^\star =
\underbrace{\arg\max\limits_\theta}_{\mathclap{\text{PG outer loop}}}
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{PG on z inner loop}}}
(\mathcal{M}_i)
$$

Augment the policy with a latent variable $z$ that will inject the structured exploration into the policy.

$$
\pi(a|s)\rightarrow\pi(a|s,\,z)
$$

1. Sample $z$, hold constant during episode
2. Adapt $z$ to a new task with gradient descent

This results in:
- Pre-adaptation: good exploration
- Post-adaptation: good task performance

Temporally extended exploration with MAESN (Gupta et al. 2018) showed much better exploration policies than MAML.

### Method Comparison

|                        | Recurrent | Gradient (MAML) | Structured Exploration (MAESN) |
|------------------------|-----------|-----------------|--------------------------------|
| Consistent             | ✘         | ✔               | ✔                              |
| Expressive             | ✔         | ✘               | ✘                              |
| Structured Exploration | ∼         | ∼               | ✔                              |
| Efficient & Off-Policy | ✘         | ✘               | ✘                              |
The recurrent method, MAML, and MAESN methods all use policy gradient and it is very sample inefficient partially because it is on-policy and must collect new data every time it wants to do an update. In single-task RL off-policy algorithms are 1-2 orders of magnitude more efficient and achieve very similar performance. This is critical for real-world applications (1 month -> 10 hours).

### Why is off-policy meta-RL difficult?

This is currently an unresolved question.

One reason however is that the meta-training data should closely match meta-test data in all current algorithms.

### The POMDP View of Meta-RL

Meta-RL can be viewed as an MDP where the rewards and the dynamics functions are dependent on the task variable. We don't observe the task variable except during adaptation, which can be thought of as figuring out what that task is. So to put this into a POMDP formalism by writing the hidden state as the concatenation of the true state and task $$h_t=(s_t,\,\mathcal{T})$$, and then redefine observations to be the states and rewards: $$o_t=(s_t,\,r_t)$$. Here we do not observe the task any longer because we do not know the task.

Note: this would also be easy to extend to Partially Observed Meta-RL where there is another part of the state that is unobserved such as in images.

Consider a 3 state linear grid world

| S0            | S1 | S2         |
|---------------|----|------------|
| Initial State |    | Goal State |

One way to solve a POMDP for an environment with an unobserved state is to maintain a belief distribution $p(h|c)$ over what state we are in. Depending on the environment a uniform distribution over the state space may be a good prior, then if we try to go left for example and hit the wall we learn we are in the leftmost state with good certainty. 

When considering the Meta-RL POMDP the location states are replaced with task states each of which corresponds to a different MDP.

| S0             | S1             | S2             |
|----------------|----------------|----------------|
| Goal for MDP 0 | Goal for MDP 1 | Goal for MDP 2 |

Now we maintain the belief distribution $p(z|c)$ and initialize with a uniform distribution believing equally that we could be in any of the tasks. Taking the same action from state zero, move left, we get zero reward and know we are in state zero and we can update the belief because we know we are not in MDP 0. As we take more actions we get a better idea what task we are in and can act more optimally.

To use this for exploration we take a sample from the belief and act as if it was the truth. For example say we selected MDP 0 we would always choose the action left to stay in the goal state and collect rewards. This gives us a way to balance between exploration and exploitation.

#### Meta-RL with Task-Belief States

$$
\theta^\star =
\underbrace{\arg\max\limits_\theta}_{\mathclap{\text{SAC outer loop}}}
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{Stochastic encoder inner loop}}}
(\mathcal{M}_i)
$$

We maintain a posterior belief over the task $p(z|c)$ where $c$ denotes context or adaptation data $\phi$. Then our agent will use samples from that belief to explore $Q_\theta(s,a,z)$. Now $f_\theta$ which was previously a RNN or Policy Gradient is a stochastic encoder.

We cannot infer the true posterior $p(z|c)$ in practice because it is intractable so we are going to use variational inference. We estimate the posterior with $q(z|c)$ and parameterize it by $\phi$ so it the distribution will be the output of some nonlinear neural network. So we get an ELBO very similar to that of Variational Bayesian Methods:

$$
\mathbb{E}_\mathcal{T}
\bigl[
\mathbb{E}_{z\sim q_\phi(z|c^\mathcal{T})}
\bigl[
\overbrace{R(\mathcal{T}, z)}^{\mathclap{\substack{\text{"Likelihood" term} \\ \text{(Bellman error)}}}}
+ \beta
\underbrace{
D_{\text{KL}}
\overbrace{
(q_\phi(z|c^\mathcal{T}) ||p(z))
}^{\mathclap{\substack{\text{Variational approximations} \\ \text{to posterior and prior}}}}
}_{\mathclap{\text{"regularization" term / information bottleneck}}}
\bigr]
\bigr]
$$

Here we are trying to maximize the likelihood term subject to the regularization constraint and the likelihood is supposed to give us a belief over the task. In this case for supervision of the belief over the task we adopt the Bellman error. While Bellman error is not a likelihood see "Control as Inference" (Levine 2018) for justification of thinking of Q as a pseudo-likelihood. Another point of view can be that the problem is an information bottleneck and we want to maximize the $R$ term and pass as much information as possible through $z$, subject to the $KL$ constraint, such that we get good predictions for $R$. The $KL$ term tries to maximize the entropy trying to limit the amount of information as the bellman error tries to maximize it. These competing terms prevent passing more information than required.

When designing the encoder, because of the **Markov property** we do not need to know the order of the transitions in order to identify the MDP. Our goal is just to infer a belief over the task so all we need is $(s,a,s^\prime,r)$ and that defines the reward and transition functions so we can use a **permutation-invariant encoder** for simplicity and speed. We take each tuple and encode it independently to get a Gaussian factor and multiply them to get a Gaussian posterior. Gaussians make the reparameterization trick nice. Using this instead of a RNN results in a system that is faster to train, more stable to train, and easy to integrate.

##### Integrating Task-Belief with Soft Actor-Critic (SAC) (Rakelly & Zhou et al. 2019)

"Soft" idea: Maximize rewards **and** entropy of the policy (higher entropy policies explore better).

$$
J(\pi) =
\sum_{t=0}^T
\mathbb{E}_{(\mathrm{s}_t,\mathrm{a}_t)\sim p_\pi}
\left[
r(\mathrm{s}_t,\,\mathrm{a}_t) +
\alpha\mathcal{H}(\pi(\cdot|\mathrm{s}_t))
\right]
$$

"Actor-Critic": Model **both** the actor (aka the policy) and the critic (aka the Q-function)

$$
J_\pi(\phi) =
\mathbb{E}_{\mathrm{s}_t,\mathrm{a}_t}
\left[
	Q_\theta(\mathrm{s}_t,\,\mathrm{a}_t) +
    \alpha\mathcal{H}(\pi_\phi(\cdot|\mathrm{s}_t))
\right]
$$
$$
J_Q(\theta) =
\mathbb{E}_{(\mathrm{s}_t,\mathrm{a}_t)\sim\mathcal{D}}
\left[
    \frac{1}{2}
    \left(
    	Q_\theta(\mathrm{s}_t,\,\mathrm{a}_t) -
        \hat{Q}(\mathrm{s}_t,\,\mathrm{a}_t)
    \right)^2
\right]
$$

See: "Control as Inference" (Levine 2018) and SAC Haamoja et al. 2018, SAC BAIR Blog post 2019.

This algorithm is currently the highest performing and most sample efficient. Works well on humanoid and manipulation tasks (see DClaw robot).

We maintain a replay buffer as we collect data and optimize both the actor and the critic with their losses. We include our task belief distribution by taking the approximate belief posterior $q_\phi(\mathbf{z}|c)$ and we pass samples from it to the actor $Q_\theta(\mathbf{s},\mathbf{a},\mathbf{z})$ and critic $\pi_\theta(\mathbf{a}|\mathbf{s},\mathbf{z})$. The state that the actor and critic receive are the concatenation of the original state of the MDP with $\mathbf{z}$ where $\mathbf{z}$ represents a task ID in some sense. They are each then updated using their respective loss functions $\mathcal{L}_\text{actor}$ and $\mathcal{L}_\text{critic}$ by backpropigation through the encoder, which is the Bellman error likelihood term.

Results:
- Simulated via MuJoCo (Todorov et al. 2012)
- Tasks proposed by: Finn et al. 2017, Rothfuss et al. 2019
- Algorithms compared: ProMP (Rothfuss et al. 2019), MAML (Finn et al. 2017), RL2 (Duan et al. 2016)
- PEARL (this algorithm) is 20-100x more sample efficient
- With single task SAC policy we are able to have a humanoid walk
- With meta-RL SAC we cannot train a humanoid to walk

This helps us by separating the data we use to infer the task with $\phi$ from the data used to train the RL agent. This addresses the distribution shift by making the adaptation data on-policy but the rest of the data used to train the actor-critic off-policy. It gives us a knob to turn and adjust how off-policy we can be to achieve the agents goals.

Limits of posterior sampling:
- Possible to show a regret bound
- Not optimal

### Discussion

Meta-World: A Benchmark and Evaluation for Multi-Task and Meta Reinforcement Learning
- 50 quite different robot manipulation tasks in MuJoCo
- Evaluated on the same algorithms as presented here

In general off-policy RL is less stable than on-policy. Problems:
1. Need to learn from data not from the current distribution
2. Meta-training optimization is itself hard

The optimization is hard even in a multi-task setting forgetting the adaptation. When training a policy that can solve 50 quite different tasks is in itself hard. One thing that can happen is the gradients from the different tasks can interfere with each other and lock preventing the model from learning. A kind of learning deadlock or thrashing.

## Model-based RL

Goal: Fitting the dynamics model of the environment.
Motivation: Often more sample efficient and model can be reused for different tasks.

This is a supervised learning problem where we try to estimate $p(\mathrm{s^\prime}|\mathrm{s},\mathrm{a})$ using $f_\phi(\mathrm{s},\mathrm{a})$.

How this is applied in multi-task and meta RL depends on if you know the form of the rewards $r_i(\mathrm{s,a})$ for each task. If yes learn a single model, plan w.r.t. each $r_i$ at test time (Deep Dynamics Models for Learning Dexterous Manipulation CoRL'19). This model-based method was sample efficient enough to train on real hardware and they were able to get a real shadow robot hand to perform the task. If we do not know the reward function:

- Multi-task RL: learn $r_\theta(\mathrm{s,a,z}_i)$ use it to plan
- Meta-RL: meta-learn $r_\theta(\mathrm{s,a},\mathcal{D}_i^\text{tr})$ use it to plan

See: Xie, Singh, Levine, Finn. Few-Shot Goal Inference for Visuomotor Learning and Planning. CoRL'18
- $\mathcal{D}_i^\text{tr}$: a few positive examples
- Use it to acquire a binary reward $r_\theta$

### Only have access to high-dimensional observations (images)

- No reward function only observations
- Need to learn the reward function

One option is to learn an image classifier, another is to provide an image of the goal (i.e. goal-conditioned RL). Approaches:

1. Learn model in latent space
2. Learn model of observations (e.g. video)
3. Predict alternative quantities

#### Latent Space

Key idea: Learn embedding $g(\mathrm{o}_t)$, then do model-based RL in latent space.
- Embed to Control: A locally linear latent dynamics model for control from raw images (NIPS 2015)
- Deep spatial autoencoders for visuomotor learning (ICRA 2016)

1. Run some policy (i.e. random policy) to collect data $\mathcal{D} = \{(\mathrm{s,a,s^\prime})_i\}$
2. Learn latent embedding of observations $s_t=g(\mathrm{o}_t)$ and model $s^\prime=f_\theta (\mathrm{s,a})$
3. Use model $f_\phi(\mathrm{s,a})$ to optimize action sequences
4. Execute planed actions, appending visiting tuples $(\mathrm{s,a,s^\prime})$ to $\mathcal{D}$

Reward signal: $r(\mathrm{o,a})=r(\mathrm{a})+\lvert\lvert g(\mathrm{o})-g(\mathrm{o}_\text{goal})\rvert\rvert$

Note that this assumes that distance is a valid metric in the latent space.

One approach to optimize the latent embedding is to learn embedding & model jointly (Watter et al. NIPS'15). Another is to use an embedding that is smooth and structured (Finn et al.'16).

These are generative methods that reconstruct the image and we do not just learn the embedding and model w.r.t. model error because if the embedding is always the same thing then it is very easy to predict and achieve perfect model error while not being useful.

Pros:
- Learn complex visual skills very effectively
- Structured representation enables effective learning

Cons:
- Reconstruction objectives might not recover the right representations

Aside: Low-dimensional embedding can also be useful for model-free approaches (FQI in latent space, Lange et al.'12) (TRPO in latent space, Ghadirzadeh et al.'17). It is also possible to use embedding for reward functions (Sermant et al. RSS'17). One way to solve the degenerate solution of optimizing both the embedding and model on the model error is to (if you have a reward) predict the reward to form better latent space (Jaderberg et al.'17, Shelhamer et al.'17). If there isn't a good reward function it may not be useful (for example if you only have a goal image).

### Modeling directly in observation (image) space

1. Run base policy $\pi_0(\mathrm{a}_t|\mathrm{o}_t)$ to collect data $\mathcal{D}=\{(\mathrm{o,a,o^\prime})_i\}$
2. Learn model $f_\phi(\mathrm{o,a})$ to minimize $\sum_i \lvert\lvert f_\phi(\mathrm{o}_i,\mathrm{a}_i)-o_i^\prime\rvert\rvert^2$
3. Use model $f_\phi(\mathrm{o,a})$ to optimize action sequence
See: Finn et al. NIPS'16 & Levine ICRA'17, Ebert et al. CoRL'17

Learning the model (2) is very complex because it is a model of how images are transformed based on our actions. This has been studied for a while now, back in 2016 they did not work very well.

To plan we:

1. Consider potential action sequences
2. Predict the future for each action sequence
3. Pick best future & execute corresponding to action
4. Repeat 1-3 to replan in real time

This method is compute intensive the loop taking approximately 1Hz on 2-4 GPUs.

This can be viewed as a visual "model-predictive control" MPC. (Finn & Levine Deep Visual FOresight for Planning Robot Motion. ICRA'17).

Folding clothes with visual MPC w.r.t. goal, multitask pick and place, rearranging objects, covering an object with a towel, model training is self supervised: (Ebert*, Finn* Dasari, Xie, Lee, Levine. Visual Foresight, arXiv'18)

Uses Action-conditioned multi-frame video prediction via flow prediction.

Pros:
- Scales to real images
- Very limited human involvement (model training is self supervised)
- Can accomplish many tasks with single model

Cons:
- Despite real images, limited background variability (not ImageNet)
- Can't (yet) handle as complex skills as other methods
- Compute intensive at test-time

One way to increase the complexity of tasks that are achievable is adding some supervision such as demonstrations from many different tasks. One way to use the demonstrations is to append them to the dataset and use them to improve the model but it can also be used to fit a model to the demonstrator and use it to direct the data collection process or to guide the planning process (imitation).

An example of this is (Xie, Ebert, Levine, Finn. Improvisation through Physical Understanding, RSS'19).

### Predicting Alternative Quantities

Predicting "If I take a sequence of actions...":
- Will I sucessfully grasp? (can be directly observed seen)
- Will I collide? (can be directly observed)
- What will health/damage/etc. be? (can be quantified on screen)

Has a close connection to Q-learning (when reward = p(event)).

Pros:
- Only predict task-relevant quantities

Cons:
- Need to be able to manually pick quantities, must be able to strictly observe them

### Model Based Meta-RL

When the dynamics $p_i(\mathrm{s^\prime|s,a})$ change across tasks we can convert this to a meta-learning problem $p_i(\mathrm{s^\prime|s,a,\mathcal{D}_i^\text{tr}})$. See (Learning to Adapt in Dynamic Environments through Meta-RL. ICLR'19) uses meta learning to teach VelociRoACH to quickly adapt on a sliding window of train-test time to different terrains.

### Takeaways of model-based vs. model-free learning

Models:
+ Easy to collect data in a scalable way (self-supervised)
+ Easy to transfer across rewards
+ Typically requires a smaller quantity of reward-supervised data
- Models don't optimize for task performance
- Sometimes harder to learn a policy
- Often need assumptions to learn complex skills (continuity, resets)

Model-Free:
+ Makes little assumptions beyond a reward function
+ Effective for learning complex policies
- Require a lot of experience (slower)
- Harder optimization problem in multi-task setting

Ultimately we want elements of both!

## Lifelong Learning

Problem Statement:

Agents may not be given a large batch of data/tasks right off the bat.

Examples:
1. A student learning concepts in school
2. A deployed image classification system learning from a stream of images from users
3. A robot acquiring an increasingly large set of skills in different environments
4. A doctor's assistant aiding in medical decision-making

When a machine learning system is deployed it should not be fixed but able to learn and adapt to changes.

Problem variations:
- task/data order: i.i.d. vs. predictable vs. curriculum vs. adversarial
- Discrete task boundaries vs. continuous shifts vs. both
- Known task boundaries/shifts vs. unknown

The general (supervised) online learning problem:

for $t=1,\dots,n$
- observe $x_t$
- predict $\hat{y}_t$
- observe label $y_t$

Possible assumption is that data is coming from i.i.d. setting: 

$$
x_t\sim p(x),\,y_t\sim p(y|x)
$$
where $p$ is not a function of $t$.
Otherwise: $x_t\sim p_t(x),\,y_t\sim p_t(y|x)$

Streaming setting: cannot store $(x_t,y_t)$
- lack of memory
- lack of computational resources
- privacy considerations
- want to study neural memory mechanisms
True in some cases but not many.
- recall: replay buffers

If task boundaries are observable: observe $x_t,z_t$

One of the classical metrics is **regret**. We want **minimal regret** (that grows slowly with $t$ and is quantified as the cumulative loss of the learner – cumulative loss of the best learner in hindsight.

$$
\text{Regret}_T:=
\sum_1^T
\mathcal{L}_t(\theta_t) -
\min\limits_\theta
\sum_1^T
\mathcal{L}_t(\theta)
$$

This cannot be evaluated in practice but is useful for analysis. Regret that grows linearly in $t$ is trivial to obtain because if you train from scratch on each task from scratch disregarding any previous experience you will get linear regret (assuming they are of comparable difficulty). So what we look for is sub-linear regret for a metric.

Other metrics include Positive & negative transfer.

Positive forward transfer: previous tasks cause you to do better on future tasks compared to learning future tasks from scratch.

Positive backward transfer: current tasks cause you to do better on previous tasks compared to learning past tasks from scratch.

positive -> negative: better -> worse

### Terminology

- Sequential Learning Settings (these terms are not used consistently)
    - Online Learning
    - Lifelong Learning
    - Continual Learning
    - Incremental Learning
    - Streaming Data
- Distinct from sequence data (language)
- Distinct from sequential decision-making (RL) (current step affects next)

### Exercise

Given example 2, a deployed image classification system learning from a stream of images from users answer:

Problem variations:
- Task/data order: Curriculum
- Discrete task boundaries
- Known task boundaries

Considerations:
- Model performance
- Data efficiency
- Computational resources
- Memory
- Others:
    - Interpretability 
    - Fairness
    - Test time compute and memory

1. How would you set-up an experiment to develop & test your algorithm?
2. What are desirable/required properties of the algorithm?
3. How do you evaluate such a system?

Properties and considerations:
- Generate new tasks (from unlabeled data)
- Learning efficiency
- Active learning
    - Know what you don't know
    - Know what you do poorly on
- Performance
- Dynamic architecture/capacity

Evaluation and setup:
- Balance between
    - Generalization and specialization
    - Recency, frequency, coverage
- Evaluate on held-out combination of tasks

### Basic Approaches

1. With no data constraints you can store all data you have ever seen and train on it. This is called the follow the leader algorithm, if it is possible this will achieve great performance. Continuous fine-tuning can help with the computational intensity.
2. Store no data and just take a gradient step on each datapoint you observe. This is very computationally cheap, requires no memory, but is subject to **negative backwards transfer** or forgetting, possibly catastrophic forgetting. Learning process is also slow which is why we usually replay data multiple times with SGD.

### Better Approaches

(Deep Online Learning via Meta-Learning ICLR'19) frames the problem as an online inference problem: infer latent "task" variable at each time step. We take a mixture of neural networks over task variable $\mathcal{T}$, and adapt continually: $\theta_t(\mathcal{T}_i)$.

We alternate between:
- E-Step: estimate latent "task" variable at each time step $\mathcal{P(T}_t)$ Given data $\mathrm{X}_t,\mathrm{Y}_t$
- M-Step: update mixture of network parameters
    - Gradient step on each mixture element is weighted by task probability

With this approach many tasks may be collected and need to be pruned eventually.

To determine if you are in a new task you can evaluate and then reset to meta-learning initialization, then take one gradient step and see if that gives a better likelihood. So an advantage of maintaining multiple models (mixture) is that you can potentially adapt in zero-shot or one-shot. This makes recall faster compared to having a single prior, but the tradeoff is that with a single prior is that you don't have to maintain multiple sets of perameters.

Note: If neural net is random initialized, this procedure would be too slow. Having some sort of meta-learned initialization is important for online learning.

Of them methods compared online learning with MAML initialization performed the best.

For evaluation it is useful to look at which task is activated for each actual task over time.

### Modify SGD to avoid negative backward transfer from scratch

Lopez-Paz & Ranzato. Gradient Episodic Memory for Continual Learning. NeurIPS'17.

1. Store small amounts of data per task in memory
2. When making updates for new tasks, ensure that they don't unlearn previous tasks

Outperformed single-task models.

### Interesting Idea

Can we meta-learn how to avoid negative backward transfer?

Javed & white. Meta-Learning Representations for Continual Learning. NeurIPS'19

### Takeaways

There are many flavors of lifelong learning under the same name.

Defining the problem is often the hardest part.

We usually only care about problems where it is not possible to store all the data.

Very open and active area of research.