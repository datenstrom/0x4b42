# Sergey Levine (UC Berkely)

Date created: 2020-07-10 11:31:00

## Current challenges in RL

- Long training times
    - Perhaps unsupervised pre-training can solve this
- Need complex reward functions
    - Perhaps can be solved using diverity-seeking exploration
- Local optima
    - Perhaps can be solved with first obtaining coverage of what is possible of multiple behaviors

Unsupervised learning of diverse behaviors (without reward function).
- Learn skills without supervision, then use them to accomplish goals
- Learn sub-skills to use with hierarchical reinforcement learning
- Explore the space of possible behaviors

How can we prepare for an **unknown** future goal?

## Definitions

- $p(x)$: distribution (e.g. over observations $x$)
- $\mathcal{H}(p(x)) = -\mathbb{E}_x\sim p(x)[\log p(x)]$: Entropy measuring how "broad" $p(x)$ is

$$
\mathcal{I}(x;y)=
D_\mathrm{KL}(P(\mathrm{x,y} ||
p(\mathrm{x})p(\mathrm{y}))
$$

$$
= \mathbb{E}_{(\mathrm{x, y})\sim 
p(\mathrm{x,y})}
\left[
\log\frac{p(\mathrm{x,y})}{p(\mathrm{x})p(\mathrm{y})}
\right]
$$

$$
= \mathcal{H}(p(\mathrm{y})) -
\mathcal{H}(p(y|x))
$$

## Information theoretic quantities in RL

$\pi(\mathrm{s})$ States the marginal distribution of policy $\pi$

$\mathcal{H}(\pi(\mathrm{s}))$ States the marginal entropy of policy $\pi$

Example of mutual information: "empowerment" (Polani et al.)

$$
\mathcal{I}
(\mathrm{s}_{t+1}; \mathrm{a}_t) =
\mathcal{H}(\mathrm{s}_{t+1} -
\mathcal{H}(\mathrm{s}_{t+1} | \mathrm{a}_t)
$$

Measures the amount of controlled authority you have over the world. If your actions are highly predictive of your states then your actions are influencing your states in a predictable way and you have a lot of control over what goes on. Can be viewed as quantifying "control authority" in an information theoretic way.

## A distribution matching formulation of RL

Ziebart, Modeling Purposeful and Adaptive Behavior with the Principle of Maximum Entropy (2010)

Soft optimality = inference in a graphical model
- R. Kalman, A new approach to linear filtering and prediction problems, 1960
- E. Todorov, General duality between optima control and estimation, 2008

Optimal control: "Which action leads to optimal future?"
Inference: "Which action was taken, given the future was optimal?"

Policy learning = variational inference
(S. Levine, Reinforcement Learning and Control as Probabilistic Inference: Tutorial and Review, 2018)

Optimizing the maximum entropy objective:
- Use on-policy policy gradient
- Modify Q-Learning

(Haarnoja, Zhou, Hartikainen, Tucker, Ha, Tan, Kuar, Zhu, Gupta, Abbeel, L. Soft Actor-Critic Algorithms and Applications. '18)

(Haarnoja, Tang, Abbeel, L., Reinforcement Learning with Deep Energy-Based Policies. ICML 2017)

(Latent Space Policies for Hierarchical Reinforcement Learning. 2018)

(Composable Deep Reinforcement Learning for Robotic Manipulation. '18)

(Learning to Walk via Deep Reinforcement Learning. '19)

## Learning without a reward function by reaching goals

Can an agent explore unsupervised learning about the world from the internet?

(Visual Reinforcement Learning with imagined goals. '18)
(Skew-Fit: State-Covering Self-Supervised Reinforcement Learning. '19)
Results:
- Robot quickly learns how to open a door
- Is never given opening a door as a goal
- Done in a lab setting

VAE (Kingman & Welling '13)

Back to the problem, how can a robot prepare for a unknown future task. Idea exploration by hallucinating an image and then trying to achieve it.

Problems:
- It might set easy goals for itself
- Where will the latent variable model come from?
- Maybe it thinks about breaking dishes

We need to encourage diversification.

One problem that the change to sampling does for exploration vs. just sampling from a uniform distribution for exploration is that is samples _valid_ samples. Think of all the possible states in an image space for example, most of the space is just random noise, the space of possible valid images would be a very complex manifold in image space.

Idea: Generalizing to state marginal matching

We have discussed matching action distributions with maximum entropy RL and how to maximize the entropy of possible goals (skew). We can combine these ideas to match state distributions which allows us to explore diverse states under a prior over good states. For example we can tell the household robot that we don't care about states where it broke dishes, but all other states we care about.

Something that seems like it would solve this problem but doesn't is intrinsic motivation which is a common method for exploration (in non information theoretic settings).
- Incentivize policy $\pi(\mathrm{a|s})$ to explore diverse states… before seeing any reward.
- Reward visiting novel states
- If a state is visited often, it is not novel
- Add an exploration bonus to reward: $\bar{r}(\mathrm{s})=r(\mathrm{s})-\log p_\pi(\mathrm{s})$
    - Where $\log p_\pi(\mathrm{s})$ is the state density under $\pi\mathrm{(a|s)}$
 
The result is that you explore a lot of states but the policy is constantly changing and at no time is it covering your space. 

(Efficient Exploration via State Marginal Matching)
(Provably Efficient Maximum Entropy Exploration)

Why is this a good idea? Eysenbach's Theorem.
(Unsupervised Meta-Learning for Reinforcement Learning)

## Covering the space of possible skills

(Eysenbach, Gupta, Ibarz, Levine. Diversity is All You Need.)

Need $\pi(\mathrm{a|s}, z)$ where $z$ is the task index

Why can't MaxEnt RL or goal-reaching cover skillspace?
- Action entropy is not the same as state entropy
    - Agent can take different actions and land in the same states
- Reaching diverse goals is not the same as performing diverse tasks.
    - Not all behaviors can be captured by goal-reaching i.e. collision avoidance navigation to a goal state, can not be defined purely as a goal to reach a position.
- MaxEnt policies are stochastic, but not always controllable
    - We want low diversity within each $z$ and high diversity across $z$'s
    
Different skills should visit different state-space regions (not goals). We can use a diversity-promoting reward function.

$$
\pi(\mathrm{a|s}, z) =
\arg\max\limits_\pi\sum_z
\mathbb{E}_{\mathrm{s}\sim\pi(\mathrm{s}|z)}
[r(\mathrm{s}, z)]
$$

Where $\mathrm{s}$ are the reward states that are unlikely for other $z^\prime \neq z$. We can use a classifier to determine which states will be visited given an input and add it to the policy->action->environment->sate loop. Each state will go to the discriminator to predict the skill which feeds into the policy.

See also:
- Variational Intrinsic Control 2016

It turns out that this is also maximizing mutual information and makes it much easier than when doing so with goals.

## Unsupervised RL for Meta-Learning

Meta-Overfitting
- Meta-Learning requires task distributions
    - They must come from you
    - We still need to design the tasks to develop good generalization
- When there are too few meta-training tasks, we can meta-overfit
    - Really good at training task recall
    - Not good at new tasks
- Specifying task distributions is hard, especially for meta-RL
- Can we propose tasks automatically?

Unsupervised Meta-Learning pipeline:
1. Environment
3. Unsupervised Meta-RL
    - Unsupervised task aquisition
    - Meta-RL
4. Meta-Learned environment-specific RL algorithm
    - Reward function additional input
    - Fast adaptation output
6. Reward-maximizing policy

See: (Gupta, Eysenbach, Finn, Levine. Unsupervised Meta-Learning for Reinforcement Learning.)
Results: "Potentially viable"

Great Insight:
How to handle very large possible spaces? Perhaps having some idea about generalization of tasks will do. For example after breaking one or a few dishes maybe you can know what will happen if you break all the others giving confidence you mastered the skill and you can move on. This way the space is covered without physically covering the space.