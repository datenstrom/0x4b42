# Deep Multi-Task and Meta Learning Datasets

Date created: 2020-07-04 09:39:38

## Few-Shot datasets

- Omniglot
  - Solved for classification
  - 99.9% on 5-way 5-shot
  - Generation not solved
- Mini-ImageNet