# Deep Multi-Task and Meta Learning

Date created: 2020-07-04 09:39:25

## Definitions

- Expressive: The ability for f to represent a range of learning procedures
- Consistency: learned learning procedure will solve tasks with enough data
  - Guarantee that the algorithm will converge
- Uncertainty awareness: Ability to reason about ambiguity during learning

Acronyms:
- GD: Gradient Descent
- MAML: Model Agnostic Meta Learning
- MAP: Maximum a posteriori probability
- ELBO: Evidence Lower BOund
- RL: Reinforcement Learning
- MDP: Markov Decision Process
- PG: Policy Gradient
- POMDP: Partially Observed Markov Decision Process
- SAC: Soft Actor-Critic
- $\mathit{D}_\mathrm{KL}$: [Kullback–Leibler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence)
- MPC: Model-Predictive Control
- i.i.d.: Independent and Identically Distributed
- MR: Meta-Regularization

## Notation

- $$\text{Dataset:}\:\mathcal{D}$$
- $$\text{Training Dataset:}\:\mathcal{D^{\text{tr}}}$$
- $$\text{Test Dataset:}\:\mathcal{D^{\text{ts}}}$$
- State Space: $\mathcal{S}$
- Action Space: $\mathcal{A}$
- $$\text{Loss function:}\:\mathcal{L}$$
- $$\text{Task:}\:\mathcal{T}$$
- $$\text{Shared parameters:}\:\theta$$
- $$\text{Task specific parameters:}\:\phi$$
- Mutual Information: $\mathcal{I}$

### Reinforcement Learning
- $$\text{Policy:}\:\pi$$
- $$\text{Markov Decision Process:}\:\mathcal{M}$$

Note that $\pi$ will be used to represent policies both discrete and continuous.
## Equations

Task definition: $\mathcal{T}_i \triangleq \{\mathcal{S}_i, \mathcal{A}_i, p_i(\mathrm{s}_1),P_i(\mathrm{s^\prime}|\mathrm{s,a}), r_i(\mathrm{s,a})\}$

$$
\text{Supervised Meta-Learning:}\quad
\theta^\star =
\arg\min\limits_\theta
\sum_{i=1}^n
\mathcal{L}(\phi_i,\,\mathcal{D}_i^\text{ts})
\quad\text{where}\quad
\phi_i=f_\theta(\mathcal{D}_i^{\text{tr}})
$$

### Reinforcement Learning

$$
\text{Value function:}\quad
V^\pi(s_t) =
\sum_{t^\prime=t}^T
\mathbb{E}_\pi
\left[r(s_{t^\prime},\,a_{t^\prime}) \:|\: s_t\right]
$$

$$
\text{Q function:}\quad
Q^\pi(s_t,\,a_t) =
\sum_{t^\prime=t}^T
\mathbb{E}_\pi
\left[r(s_{t^\prime},\,a_{t^\prime}) \:|\: s_t,\,a_t\right]
$$

Also called the [Bellman equation](https://en.wikipedia.org/wiki/Bellman_equation):
$$
\text{Optimal policy }\pi^\star\text{:}\quad
Q^\star(s_t,\,a_t) =
\mathbb{E}_{s^\prime\sim p(\cdot\,|\,s,\,a)}
\left[
r(s,\,a)\,+\,
\gamma\max\limits_{a^\prime}
Q^\star(s^\prime,\,a^\prime)
\right]
$$

$$
\text{Meta-RL:}\quad
\theta^\star =
\arg\max\limits_\theta
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=f_\theta(\mathcal{M}_i)
$$

## Summary

Tasks must share "structure" but what does that mean? That there is some statistical dependence on shared latent information.

Key characteristic of meta-learning: the conditions at meta-training time should closely match those at test time. For example if meta train classes are dog breed classification meta-test should not be a motorcycle model but an unseen dog breed.

Architectures like black-box and MAML only assume that you can get some gradient so they are generally easy to combine with other methods like RL. Non-parametric methods I guess not? Perhaps the embedding could be updated offline (sleep) and only used as long-term memory vs. black-box and MAML being online?

Training has an outer meta-training loop and an inner adaptation-training loop for example on the generic supervised and reinforcement meta-learning equations they are shown here:

$$
\text{Supervised:}\quad
\theta^\star =
\underbrace{\arg\min\limits_\theta}_{\mathclap{\text{outer loop}}}
\sum_{i=1}^n
\mathcal{L}(\phi_i,\,\mathcal{D}_i^\text{ts})
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{inner loop}}}
(\mathcal{D}_i^{\text{tr}})
$$


$$
\text{Meta-RL:}\quad
\theta^\star =
\underbrace{\arg\max\limits_\theta}_{\mathclap{\text{outer loop}}}
\sum_{i=1}^n
\mathbb{E}_{\pi_{\phi_i}}
(\mathcal{T})
\left[
R(\mathcal{T})
\right]
\quad\text{where}\quad
\phi_i=
\underbrace{f_\theta}_{\mathclap{\text{inner loop}}}
(\mathcal{M}_i)
$$

## Open Questions

Why is off-policy meta-RL difficult?